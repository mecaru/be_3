
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% FILE: linel_T.m
% Linear elastic analyses for plane problems with linear and quadratic triangles
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

addpath(genpath('.'))

clear all

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% preprocessor phase: reads input from file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

name='fissure2';  % user sets nam of input file 
%name='barr03lin';  % user sets nam of input file
eval(['run input/',name]);  % input file is read at once 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% reads GMSH file
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

mshfile=['input/' file];
fmid=fopen(mshfile,'r'); % opens input file

tline = fgets(fmid); % reads a series of lines in the preamble of input file
tline = fgets(fmid); 
tline = fgets(fmid); 
tline = fgets(fmid); 

tline = fgets(fmid);  % this line contains the number of nodes NN
NN=sscanf(tline,'%d');  % reads NN from string

for i=1:NN,  % loop over all the lines to read nodal coordinates
 tline = fgets(fmid); 
 h=sscanf(tline,'%d %f %f %f')'; % vector h contains node number and 3 coordinates
 nodes(h(1)).coor=h(2:3);
 nodes(h(1)).dof=zeros(1,2); % initializes degree of freedom of node
 nodes(h(1)).displ=zeros(1,2); % initializes nodal displacements
end
analysis.NN=NN; % fills the field of structure analysis 

nsolid=numel(solid(:,1));  % the number of sets associated to materials is given by the num of lines of solid 
ndbc=0; ndbcn=0; ntbc=0;
if exist('dbc')  ndbc=numel(dbc(:,1)); end % the number of sets with imposed dbc is given by the num of lines of dbc
if exist('dbcn') ndbcn=numel(dbcn(:,1)); end % the number of nodes with imposed dbc is given by the num of lines of dbcn
if exist('tbc')  ntbc=numel(tbc(:,1)); end % the number of sets with imposed tbc is given by the num of lines of tbc

tline = fgets(fmid); 
tline = fgets(fmid); 

tline = fgets(fmid); % this line contains the number of elements
num=sscanf(tline,'%d'); % reads the number of elements 

NE=0;
NL=0;
for i=1:num,                         % the elset to which the element belongs, ...
 tline = fgets(fmid);                        % and the material (mat)
 h=sscanf(tline,'%d')';
 physet=h(4);     % physical set
 for j=1:nsolid   
  if solid(j,1)==physet 
   iok=1;
   NE=NE+1;
   elements(NE).type=h(2);
   elements(NE).mat=solid(j,2);
   elements(NE).nodes=h(6:length(h));
  end 
 end
 for j=1:ntbc   
  if tbc(j,1)==physet
   NL=NL+1;
   loads(NL).type=h(2);
   loads(NL).nodes=h(6:length(h));
   loads(NL).dir=tbc(j,2);
   loads(NL).val=tbc(j,3);
  end
 end    
 for j=1:ndbc   
  if dbc(j,1)==physet
   for pos=6:length(h)
    nodes(h(pos)).dof(dbc(j,2))=-1;
    nodes(h(pos)).displ(dbc(j,2))=dbc(j,3);
   end 
  end
 end
end
analysis.NE=NE;
analysis.NL=NL;

for j=1:ndbcn   
 nodes(dbcn(j,1)).dof(dbcn(j,2))=-1;
 nodes(dbcn(j,1)).displ(dbcn(j,2))=dbcn(j,3);
end    

fclose(fmid);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% defines materials
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nmat=length(mat(:,1));   % number of materials
for i=1:nmat
 E=mat(i,1);
 nu=mat(i,2);
 if atype==1                                    % plane strain
  materials(i).A=...                             % defines matrix of elastic constants
       E/((1+nu)*(1-2*nu))*[1-nu,nu,0;
                            nu,1-nu,0;
                            0,0,(1-2*nu)/2];  
 else
  materials(i).A=E/(1-nu^2)*[1,nu,0;                % plane stress
                          nu,1,0;
                          0,0,(1-nu)/2];   
 end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% defines numbering of unknown nodal values
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ltag=['B2';'T3';'Q4';'00';'00';'00';'00';'B3';'T6'];
Lne=[2 3 4 0 0 0 0 3 6];  % number of nodes per element type
Lc1=[0 2 2 0 0 0 0 0 3];  % Lc2 and Lc2 are lists of coefficients needed to estimate 
Lc2=[0 1 2 0 0 0 0 0 3];  % the number of non zero coeffs for the different element types

neq=0;
for e=1:analysis.NE,                      % assign equation numbers 
 type=elements(e).type;
 ne=Lne(type);   
 connec=elements(e).nodes; 
 for n=1:ne
  node=connec(n);   
  for d=1:2,
   if nodes(node).dof(d)==0,                     % if no dbc are enforced on the node and node is active
    neq=neq+1;                          % increments equation number
    nodes(node).dof(d)=neq;                % and assigns value to current dof
   end  
  end
 end 
end
analysis.neq=neq;


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% estimate of number of non-zero coefficients for Morse stocking
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Ek=max(Lc1)*ones(analysis.NN,1);             % initialisation of Ek
for e=1:analysis.NE,                         % loop over elements
 type=elements(e).type;
 connec=elements(e).nodes;
 Ek(connec)=Ek(connec)+Lc2(type);            % Ek is incremented on connectivity nodes
end
ncoeffs=4*sum(Ek);                           % sum of all the terms in Ek

K=spalloc(analysis.neq,analysis.neq,ncoeffs);% allocates sparse matrix
F=zeros(analysis.neq,1);                     % allocates rhs vector

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% assemblage phase: system matrix and nodal forces due to imposed displ.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------------------------
[figmess,t1,t2]=createfigmess;
set(t1,'string',['Number of elements: ' num2str(analysis.NE)]);
set(t2,'string','Assembling: 0%'); pause(.01);
percold=0;
%-----------------------------------------------------------------------------------------

D=2;                                         % problem dimensionality
for e=1:analysis.NE,                         % stiffness matrix assemblage
	
%-----------------------------------------------------------------------------------------
 perc=10*floor(10*e/analysis.NE);
 if perc~=percold,
  percold=perc;
  set(t2,'string',['Assembling: ' num2str(percold) '%']); pause(.01)
 end
%-----------------------------------------------------------------------------------------

 type=elements(e).type;
 ne=Lne(type);   
 Dne=D*ne;                           % number of nodal values in one surface el.
 EL=zeros(ne,2);
 dofe=zeros(Dne,1);
 disple=zeros(Dne,1);
 pos=1;
 for n=1:ne
  node=elements(e).nodes(n);   
  EL(n,:)=nodes(node).coor;                           % creates element
  dofe(pos:pos+D-1)=nodes(node).dof;
  disple(pos:pos+D-1)=nodes(node).displ;
  pos=pos+D;
 end
 mat=elements(e).mat;
 Ke=eval(['stiff_linel_',Ltag(type,:),... % computes element stiffness matrix
                  '(EL,materials(mat).A)']);       
 pe=find(dofe>0);                           % gets position of unknown displ. compon.
 Ie=dofe(pe);                               % gets value of associated DOFs 
 K(Ie,Ie)=K(Ie,Ie)+Ke(pe,pe);               % matrix assemblage
 pe_UDe=find(dofe<0);                       % finds pos of UDe
 UDe=disple(pe_UDe);                        % gets value of UDe
 F(Ie)=F(Ie)-Ke(pe,pe_UDe)*UDe;             % assemblage of rhs
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% assemblage phase: nodal loads due to surface tractions
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for e=1:analysis.NL,                        % for each loaded face in the model
 type=loads(e).type;
 ne=Lne(type);   
 Dne=D*ne;                           % number of nodal values in one surface el.
 idir=loads(e).dir;                            % gets direction
 val=loads(e).val;                             % gets traction value
 pos=1;
 EL=zeros(ne,2);
 dofe=zeros(Dne,1);
 for n=1:ne
  node=loads(e).nodes(n);   
  EL(n,:)=nodes(node).coor;                           % creates element
  dofe(pos:pos+D-1)=nodes(node).dof;
  pos=pos+D;
 end
 Fe=eval(['nf_tractions_',Ltag(type,:),...
			                '(EL,val,idir)']); 
 pe=find(dofe>0);                           % finds non-zero entries of dofe
 Ie=dofe(pe); 
 F(Ie)=F(Ie)+Fe(pe);                        % adds to global rhs
end 
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Solution phase
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%-----------------------------------------------------------------------------------------
set(t2,'string',['Solving system']); pause(.01)
%-----------------------------------------------------------------------------------------

U=K\F;                                       % solution of linear system

%clear K F                                    % clears memory

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing phase: computation of deformation energy
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Calcul de l'�nergie interne
Eint = U'*K*U;



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing phase: computation of nodal stresses via extrapolation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for n=1:analysis.NN
 for dir=1:2
  dof=nodes(n).dof(dir);
  if dof>0 
   nodes(n).displ(dir)=U(dof);
  end 
 end
end

stress=zeros(analysis.NN,3);                 % initializes "stress"
counter=zeros(analysis.NN,1);                % initializes "counter"
for e=1:analysis.NE,                         % computes nodal stresses
 type=elements(e).type;
 ne=Lne(type);   
 Dne=D*ne;                           % number of nodal values in one surface el.
 EL=zeros(ne,2);
 disple=zeros(Dne,1);
 connec=elements(e).nodes;
 pos=1;
 for n=1:ne
  node=connec(n);   
  EL(n,:)=nodes(node).coor;                           % creates element
  disple(pos:pos+D-1)=nodes(node).displ;
  pos=pos+D;
 end
 mat=elements(e).mat;
 stressG=eval(['stressG_linel_',...        % computes stresses in one element ...
                Ltag(type,:),'(EL,materials(mat).A,disple)']);  % at gauss points
 stressN=eval(['G2N_',...                  % and extrapolates to vertex nodes 
                Ltag(type,:),'(EL,stressG)']);   
 stress(connec,:)=stress(connec,:)+stressN;   % adds stresses to triangle nodes
 counter(connec)=counter(connec)+1;
end   
for icomp=1:3
 stress(:,icomp)=stress(:,icomp)./counter;  % naive average of stresses
end

%-----------------------------------------------------------------------------------------
close(figmess)
%-----------------------------------------------------------------------------------------

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing phase: writes data on file for post-processing wih GMSH
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% prepares output file
fmid=fopen('input/out.msh','w');
fprintf(fmid,'%s\n%s\n%s\n','$MeshFormat','2.2 0 8','$EndMeshFormat');

% displacement output
fprintf(fmid,'%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n','$NodeData','1','"vec U"','1','0.0','3','0','3');
fprintf(fmid,'%d\n',analysis.NN);
for n=1:analysis.NN
 fprintf(fmid,'%d %E %E %E\n',n,nodes(n).displ(1),nodes(n).displ(2),0);   
end
fprintf(fmid,'%s\n','$EndNodeData');

% stress output
fprintf(fmid,'%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n','$NodeData','1','"stress"','1','0.0','3','0','9');
fprintf(fmid,'%d\n',analysis.NN);
for n=1:analysis.NN
 fprintf(fmid,'%d %E %E %E %E %E %E %E %E %E\n',n,stress(n,1),stress(n,2),stress(n,3),0,0,0,0,0,0);   
end
fprintf(fmid,'%s\n','$EndNodeData');
fclose(fmid);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing phase: computation of K1
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Extraction des d�placements nodaux le long de la fissure proche de la
% pointe

deplacements = []; % D�placements selon y des points proches de la pointe
rayons = []; % Rayons associ�s
for n=1:NN 
   coords = nodes(n).coor;
   x = coords(1);
   y = coords(2);
   seuil = 0.005/10;
   % S�lection des noeuds de la fissure proches de la pointe excluant la
   % pointe
   if (y==0) & (x<0) & (abs(x)<seuil) & (x~=0)
       uy = nodes(n).displ(2); % D�placement du noeud selon y
       rayons = [rayons x];
       deplacements = [deplacements uy];
   end
end

sauts = 2*abs(deplacements); % Sauts de uy � travers la fissure
list_K = E/(8*(1-nu^2))*sauts.*sqrt(2*pi./abs(rayons));
K = mean(list_K);

% Valeur th�orique
a = 0.005;
L = 0.01;
C = 1.12-0.231*a/L+10.55*(a/L)^2-21.72*(a/L)^3+30.39*(a/L)^4; % Facteur de correction g�om�trique
Kth = C*abs(sigma)*sqrt(pi*a);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Post-processing phase: computation of G
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
G = 0;

% Parametres geometriques
r0 = a/10;
r1 = a/5;
for element=1:NE
    % Recherche des elements dans la couronne 
    inside = true; % Vrai si l'element est dans la couronne
    nodes_e = elements(element).nodes;
    ne = length(nodes_e); % Nombre de noeuds dans l'element
    T = zeros(ne,2); % Coordonnees des noeuds de l'element
    u_e = zeros(ne,2); % Deplacements des noeuds de l'element
    theta_e = zeros(ne,2); % Valeurs de theta aux noeuds de l'element
    for node=1:ne
       coords = nodes(nodes_e(node)).coor;
       u = nodes(nodes_e(node)).displ;
       T(node,:) = coords;
       u_e(node,:) = u;
       distance = sqrt(coords(1)^2 + coords(2)^2);
       theta = (distance-r1)/(r0-r1);
       theta_e(node,:) = coords*theta;
       if (distance>r1*1.2) | (distance<r0*0.8)
           inside = false;
       end
    end
    if inside==true
        % Calcul du G elementaire
        Ge = calcul_G_T3(T,materials.A,u_e,theta_e);
        G = G + Ge;
    end
end
G = 2*G
G_irwin = (1-nu^2)/E*K^2


%dos(['gmsh/Gmsh.app/Contents/MacOS/gmsh input/',name,'.msh input/out.msh input/options.geo']);  % runs gmsh with a dos command
%dos(['gmsh/gmsh.exe input/',name,'.msh input/out.msh input/options.geo']);  % runs gmsh with a dos command
dos(['/usr/bin/gmsh input/',name,'.msh input/out.msh input/options.geo']);