
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Stresses at gauss points: T6 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function stressG=stressG_linel_T6(T,A,Ve)

a_gauss=1/6*[4 1 1; 1 4 1; 1 1 4]; 
stressG=zeros(3,3);
for g=1:3,
  a=a_gauss(g,:);                            % area coord. for gauss point
  DN=[4*a(1)-1 0 -4*a(3)+1 4*a(2)...         % derivative of shape functions...
      -4*a(2) 4*(a(3)-a(1));                 % w.r.t. a_1,a_2
      0 4*a(2)-1 -4*a(3)+1 4*a(1) ...
      4*(a(3)-a(2)) -4*a(1)]';
  J=T'*DN;                                   % jacobian matrix
  detJ=J(1,1)*J(2,2)-J(1,2)*J(2,1);          % jacobian
  invJ=1/detJ*[ J(2,2) -J(1,2); ...          % inverse jacobian matrix
               -J(2,1)  J(1,1)];
  GN=DN*invJ;                                % gradient of shape functions
  Be=[GN(1,1) 0 GN(2,1) 0 GN(3,1) 0 ...
      GN(4,1) 0 GN(5,1) 0 GN(6,1) 0;
      0 GN(1,2) 0 GN(2,2) 0 GN(3,2)...
      0 GN(4,2) 0 GN(5,2) 0 GN(6,2);
      GN(1,2) GN(1,1) GN(2,2) GN(2,1)...
      GN(3,2) GN(3,1) GN(4,2) GN(4,1)...
      GN(5,2) GN(5,1) GN(6,2) GN(6,1)];
  stressG(g,:)=(A*Be*Ve)';
end

