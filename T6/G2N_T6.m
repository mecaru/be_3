
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Extrapolation from Gauss points to nodes: T6 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function qN=G2N_T6(T,qG)

N=1/9*[2 -1 -1 4 1 4;
       -1 2 -1 4 4 1;
       -1 -1 2 1 4 4];
xg=[ones(3,1) N*T];                     % physical coordinates of gauss points
M=[ones(6,1) T]; 
qN=M*(xg\qG);                           % linear extrapolation to nodes


