
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Nodal forces due to surface tractions: B2
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function Fe=nf_tractions_B2(L,val,idir)

a_gauss=[0];                                % gauss integration
w_gauss=[2];
Fe=zeros(4,1);
for g=1:1                                   % loop over gauss points
 a=a_gauss(g);
 DN=[-1/2 1/2];                      % derivative of shape functions
 J=DN*L;
 detJ=sqrt(J(1)^2+J(2)^2);                  % jacobian
 TD=zeros(2,1);
 if idir>0,                                 % sets traction vector
  TD(idir)=val;
 else
  TD=val/detJ*[J(2) -J(1)]';                % traction along normal vector
 end
 NL=[(1-a)/2 (1+a)/2];          % shape functions
 N=[NL(1) 0 NL(2) 0 ;
    0 NL(1) 0 NL(2) ];
 Fe=Fe+N'*TD*detJ*w_gauss(g);
end

