
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Linear elastic stiffness matrix: T6
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function Ke=stiff_linel_T3(T,A)

a_gauss=1/3;                                 % Gauss abscissae
w_gauss=[1/2];                               % Gauss weights
Ke=zeros(6,6);
for g=1:1,                                   % loop over Gauss points
  a=a_gauss(g,:);                            % param. coordinates for gauss point
  DN=[1 0 -1;                                % derivative of shape functions...
      0 1 -1]';                              % w.r.t. a_1,a_2
  J=T'*DN;                                   % jacobian matrix
  detJ=J(1,1)*J(2,2)-J(1,2)*J(2,1);          % jacobian
  invJ=1/detJ*[ J(2,2) -J(1,2); ...          % inverse jacobian matrix
               -J(2,1)  J(1,1)];
  GN=DN*invJ;                                % gradient of shape functions
  Be=[GN(1,1)   0       GN(2,1)     0       GN(3,1) 0;
      0         GN(1,2) 0           GN(2,2) 0       GN(3,2);
      GN(1,2)  	GN(1,1) GN(2,2)     GN(2,1) GN(3,2) GN(3,1)];
  Ke=Ke+Be'*A*Be*detJ*w_gauss(g);            % contribution to stiffness matrix
end

