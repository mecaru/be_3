
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% FILE: nf_gravity_T3.m
% Nodal forces due to gravity along e2 direction: T3
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function Fe=nf_gravity_T3(T,rhog)

a_gauss=1/3;                                 % Gauss abscissae
w_gauss=1/2;                               % Gauss weights
Fe=zeros(6,1);
for g=1:1,                                   % loop over Gauss points
  a=a_gauss(g,:);                            % param. coordinates for gauss point
  N=[a 0 a 0 1-2*a 0;
     0 a 0 a 0 1-2*a ];                  % shape functions
  DN=[1 0 -1;                                % derivative of shape functions...
      0 1 -1]';                              % w.r.t. a_1,a_2
  J=T'*DN;                                   % jacobian matrix
  detJ=J(1,1)*J(2,2)-J(1,2)*J(2,1);          % jacobian
  
  Fe=Fe+([0 -rhog]*N*detJ*w_gauss(g))';      % contribution to second member
end

