
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Contribution elementaire de G: T3
% TODO : Calcul de la contribution elementaire
% TODO : dans le main, calcul de theta_e, extraction des elements de la
% couronne, somme des G elementaires
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function Ge=calcul_G_T3(T,A,u_e,theta_e)
%u_e et theta_e sont les vecteurs depalcements et theta aux noeuds, ils
%sont sous la forme de matrices 3X2 : 
% [ux1 uy1;
%  ux2 uy2;
%  ux3 uy3]

a_gauss=1/3;                                 % Gauss abscissae
w_gauss=[1/2];                               % Gauss weights
Ge = 0;                                      % Initialisation de Ge
for g=1:1,                                   % loop over Gauss points
  a=a_gauss(g,:);                            % param. coordinates for gauss point
  DN=[1 0 -1;                                % derivative of shape functions...
      0 1 -1]';                              % w.r.t. a_1,a_2
  J=T'*DN;                                   % jacobian matrix
  detJ=J(1,1)*J(2,2)-J(1,2)*J(2,1);          % jacobian
  invJ=1/detJ*[ J(2,2) -J(1,2); ...          % inverse jacobian matrix
               -J(2,1)  J(1,1)];
  GN=DN*invJ;                                % gradient of shape functions
  % Matrice permettant le calcul de grad(u)
  grad=[GN(1,1)   GN(2,1) GN(3,1);
          GN(1,2)   GN(2,2) GN(3,2)];
  
  nabla_u = (grad*u_e)'; % valeur de la matrice grad(u) telle que definie dans le sujet
  nabla_theta = (grad*theta_e)'; % valeur de la matrice grad(theta) telle que definie dans le sujet
  
  % Premiere partie de Ge
  Ge1 = produit_contracte(nabla_u*nabla_theta, A, nabla_u);
  
  % Seconde partie de Ge
  div_theta = grad(1,:)*theta_e(:,1) + grad(2,:)*theta_e(:,2);
  Ge2 = -0.5*produit_contracte(nabla_u,A,nabla_u)*div_theta;
  
  Ge = Ge + (Ge1 + Ge2)*detJ*w_gauss(g);      % Contribution a la valeur de Ge
  
end

