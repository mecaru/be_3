// global dimensions

H=0.01;
L=0.01;
a=0.5*L;
r0=a/10;
r1=a/2;


// mesh parameters

 
lc1=.0005;
lc2=.00005;



// point coordinates

Point(1) = {0,0,0,lc2};
Point(2) = {-a,0,0,lc1};
Point(3) = {-a,-H,0,lc1};
Point(4) = {L-a,-H,0,lc1};
Point(5) = {L-a,0,0,lc1};
Point(6) = {-r0,0,0,lc2};
Point(7) = {r0,0,0,lc2};
Point(8) = {-r1,0,0,lc1};
Point(9) = {r1,0,0,lc1};

// lines and line loops

Line(1) = {1,6};
Circle(2) = {6,1,7};
Line(3) = {7,1};
Line Loop(4) = {1,2,3};

Line(5) = {6,8};
Circle(6) = {8,1,9};
Line(7) = {9,7};
Line Loop(8) = {5,6,7,-2};

Line(9) = {8,2};
Line(10) = {2,3};
Line(11) = {3,4};
Line(12) = {4,5};
Line(13) = {5,9};
Line Loop(14) = {9,10,11,12,13,-6};



// surface

Plane Surface(1) = {4};
Plane Surface(2) = {8};
Plane Surface(3) = {14};


// physical entities

Physical Line(1) = {11}; 
Physical Line(2) = {13,7,3}; 
Physical Surface(3) = {1,2,3}; 

