file='fissure2.msh';

sigma = -1.E6;

% ANALYSIS TYPE
atype=2; % Contraintes planes

% MATERIAL: Young and Poisson
mat= [210E6 0.3]; 

% SOLID: associates materials to sets
solid = [3 1];

% DBC: each row a bc. Physical set, direction, val
dbc= [2 2 0];
  
% nodal DBC: each row a bc. node, direction, val
dbcn=[3 1 0];  

% TBC: each row a bc. Physical set, direction, val
tbc = [1 2 sigma];




