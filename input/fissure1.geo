// global dimensions

H=0.01;
L=0.01;
a=0.5*L;
r0=a/10;
r1=a/2;


// mesh parameters

 
lc1=.0005;
lc2=.00005;


// point coordinates

Point(1) = {0,0,0,lc2};
Point(2) = {-a,0,0,lc1};
Point(3) = {-a,-H,0,lc1};
Point(4) = {L-a,-H,0,lc1};
Point(5) = {L-a,0,0,lc1};

// lines and line loops

Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,5};
Line(5) = {5,1};
Line Loop(6) = {1,2,3,4,5};

// surface

Plane Surface(1) = {6};

// physical entities

Physical Line(1) = {3}; 
Physical Line(2) = {5}; 
Physical Surface(3) = {1}; 

