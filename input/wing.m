file='wing.msh';

% ANALYSIS TYPE
atype=1;

% MATERIAL: Young and Poisson
mat= [1 1/3];

% SOLID: associates materials to sets
solid = [3 1];

% DBC: each row a bc. Physical set, direction, val
dbc= [2 1 0;
      2 2 0];

% TBC: each row a bc. Physical set, direction, val
tbc = [1 2 0.0625];




