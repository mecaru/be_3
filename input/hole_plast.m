file='hole.msh';

%ANALYSIS TYPE
atype=4; %plasticite
history=[ 1. 2. 3. 4. 5. 6. 7. 8.];


%MATERIAL TYPE ISOTROPIC
mat=[1 0.3 0. 0.88];  %Young, Poisson, Hardening, Sigma0

% SOLID: associates materials to sets
solid = [4 1];

%Dirichlet boundary conditions on sets
dbc= [1 2 0;
      3 1 0;
      2 1 1];

%Dirichlet boundary conditions on nodes
%dbcn=[1 2 0];

