file='strip.msh';

%ANALYSIS TYPE
atype=4; %plasticite
history=[0. 1. 2. 2.5 2.75 3. 3.5 4. 5. 6];


%MATERIAL TYPE ISOTROPIC
mat=[1 0.1 0. 1.];  %Young, Poisson, Hardening, Sigma0

% SOLID: associates materials to sets
solid = [3 1];

%Dirichlet boundary conditions on sets
dbc= [1 1 1;
      2 1 0];

%Dirichlet boundary conditions on nodes
dbcn=[1 2 0];

