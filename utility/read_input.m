%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% FILE read_input.m
% Reads input file and mesh
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [analysis,materials,connec,coor,dof,displ,TD]=read_input(pname,fname,t2)

fid=fopen([pname fname],'r');                % opens input file
tline = fgets(fid); 
mname=sscanf(tline,'%s');

%--------------------------------------------------------------------------
set(t2,'string',['Reading .msh']);
pause(.01)
%--------------------------------------------------------------------------

[analysis,elements,corrispnod,coor]= ...     % reads nodes and elem. from GMSH file
                     input_mesh(pname,mname);

%--------------------------------------------------------------------------
set(t2,'string',['Reading .inp']);
pause(.01)
%--------------------------------------------------------------------------

tline = fgets(fid); 
while isempty(strfind(tline,'*ENDFILE')),
 if strfind(tline,'*ANALYSIS')
  tline=tline(11:length(tline));
  [analysis,dof,displ]=...                   % ANALYSIS section
          input_analysis(fid,tline,analysis);
 elseif strfind(tline,'*MATERIAL')
  tline=tline(11:length(tline));
  materials=...                              % MATERIALS section
          input_material(fid,tline,analysis);
 elseif strfind(tline,'*SOLID')
  [connec,analysis]=...                      % SOLID section
    input_solid(fid,tline,elements,analysis);
 elseif strfind(tline,'*DBC')
  tline=tline(6:length(tline));
  [dof,displ]=...                            % DBC section
        input_dbc(fid,tline,elements,dof,...
                  displ,analysis,corrispnod);
 elseif strfind(tline,'*TBC')
  tline=tline(6:length(tline));   
  [analysis,TD]=...                          % TBC section
       input_TD(fid,tline,elements,analysis);
 elseif strfind(tline,'*INITIAL')
  tline=tline(10:length(tline));   
  [analysis]=...                             % INITIAL section
           input_initial(fid,tline,analysis);
 end 
 tline = fgets(fid); 
end

clear elements corrispnod
fclose(fid);                                 % closes file

%--------------------------------------------------------------------------
set(t2,'string',['setting DOF']);
pause(.01)
%--------------------------------------------------------------------------

analysis.neq=0;
nodel=length(connec(1,:));                   % number of nodes in one element

for inod=1:analysis.NN,                      % assign equation numbers 
 for idir=1:2,
  if dof(inod,idir)==0,                      % if no dbc enforced on the node
   analysis.neq=analysis.neq+1;              % increments equation number
   dof(inod,idir)=analysis.neq;              % and assigns value to current dof
  end  
 end
end

if exist('TD')==0,                           % initialisation of TD structure
 TD=0;
 analysis.nTD=0;
end 


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads traction boundary conditions 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [analysis,TD]=input_TD(fid,tline,elements,analysis)

analysis.nTD=0;
tline = fgets(fid); 
while ~strcmp(tline(1:2),'**'),
 h=strfind(tline,'VAL=');
 val=sscanf(tline(h+4:length(tline)),'%f');  % reads value of TBC
 tline=tline(1:h-2);
 h=strfind(tline,'DIR=');
 dir=sscanf(tline(h+4:length(tline)),'%d');  % reads direction of TBC
 tline=tline(1:h-2);
 h=strfind(tline,'ELSET=');
 elset=sscanf(tline(7:length(tline)),'%d');  % reads name of elset on which ...                                                 
 ic=zeros(analysis.NE,1);                    % TBC is exerted
 nTD=0;
 for i1=1:analysis.NE,                       % counts elements of elset
  if(elements(i1).elset==elset),
   nTD=nTD+1;   
   ic(nTD)=i1;   
  end    
 end
 TD2=repmat(struct('dir',0,'val',0,...       % initialization of TD2
            'nodes',[]),1,nTD);
 for i=1:nTD,                                % copies information to each laoded face
  nodes=elements(ic(i)).nodes;   
  TD2(i).nodes=nodes;
  TD2(i).dir=dir;
  TD2(i).val=val;
 end
 if analysis.nTD==0,                         % if it is the first elset loaded...
  TD=TD2;
 else                                        % if it is not the first elset loaded ..
  TD=[TD TD2];                               % combines the different TD
 end
 analysis.nTD=analysis.nTD+nTD;
 tline = fgets(fid); 
end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads displacement boundary conditions 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [dof,displ]=input_dbc(fid,tline,elements,dof,displ,analysis,corrispnod)

tline = fgets(fid); 
while ~strcmp(tline(1:2),'**'),
 h=strfind(tline,'VAL=');
 val=sscanf(tline(h+4:length(tline)),'%f');  % reads value of displacement imposed
 tline=tline(1:h-2);
 h=strfind(tline,'DIR=');
 dir=sscanf(tline(h+4:length(tline)),'%d');  % reads direction
 tline=tline(1:h-2);
 if(~isempty(strfind(tline,'ELSET='))),      % if condition imposed on nodes of elset
  elset=sscanf(tline(7:length(tline)),'%d'); % reads elset 
  ic=find([elements.elset]==elset);
  for i=1:length(ic),                        % for all the nodes of the elset 
   nodes=elements(ic(i)).nodes;   
   dof(nodes,dir)=...                        % fills with position in equivalent ...
              -(analysis.NN*(dir-1)+nodes)'; % 1D vector
   displ(nodes,dir)=val;                     % copies value of imposed disp
  end
 elseif(~isempty(strfind(tline,'NODE='))),   % if condition imposed only on one node
  nodeGMSH=sscanf(tline(6:length(tline)),'%d');
  node=corrispnod(nodeGMSH);
  dof(node,dir)=-(analysis.NN*(dir-1)+node); % fills with position in equiv. vector
  displ(node,dir)=val;                       % copies value of imposed fisp
 end
 tline = fgets(fid); 
end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads solid elset 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [connec,analysis]=input_solid(fid,tline,elements,analysis)
 
tline = fgets(fid); 
h=strfind(tline,'ELSET=');                  
elset=sscanf(tline(h+6:length(tline)),'%d'); % gets name of elset
nel=length(elements);
ic=zeros(nel,1);
analysis.NE=0;
for i1=1:nel,                                % counts elements in elset
 if(elements(i1).elset==elset),
  analysis.NE=analysis.NE+1;   
  ic(analysis.NE)=i1;         
 end    
end
numnodes=length(elements(ic(1)).nodes);
connec=zeros(analysis.NE,numnodes);
for i=1:analysis.NE,                         % for all the active elements
 connec(i,:)=elements(ic(i)).nodes;          % copies connectivity in connec
end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads analysis section 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [analysis,dof,displ]=input_analysis(fid,tline,analysis)

tline = fgets(fid); 
h=strfind(tline,'TYPE=');                    
analysis.type=...
    sscanf(tline(h+5:length(tline)),'%s');
switch analysis.type                         % prepares data structure for each analysis
 case {'PLANESTRAIN','PLANESTRESS','AXI'...  % if continuum mechanics analysis
       'PLAST','GDISP','DYNAMIC'}            
  dof=zeros(analysis.NN,2);                  % two components at each node node 
  displ=zeros(analysis.NN,2);
 case {'DIFF'}                               % if diffusion analysis 
  dof=zeros(analysis.NN,1);                  % one component at each node
  displ=zeros(analysis.NN,1);
 otherwise
  disp('Unknown analysis type')
end
if strcmp(analysis.type,'AXI'),              % if axisymm... 
 analysis.Etag=[analysis.Etag 'axi']; 
end
if strcmp(analysis.type,'PLAST'),            % if plastic analysis... 
 history=fscanf(fid,'%f');                   % reads load history           
 analysis.numstep=length(history);
 analysis.history=[0 history'];
elseif strcmp(analysis.type,'DYNAMIC'),      % dynamics analysis 
 tline = fgets(fid); 
 analysis.tf= ...
      sscanf(tline(7:length(tline)),'%f');   % reads final time
 tline = fgets(fid); 
 analysis.dt= ...
      sscanf(tline(4:length(tline)),'%f');   % reads time step 
 tline = fgets(fid); 
 analysis.penalty=...                        % reads penalty factor
      sscanf(tline(9:length(tline)),'%f');
 tline = fgets(fid); 
 analysis.gravity=...
      sscanf(tline(6:length(tline)),'%f');   % reads gravity
end
 
 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads material properties 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function materials=input_material(fid,tline,analysis)

h=strfind(tline,'TYPE=');
materials.type=tline(h+5:length(tline));
tline=tline(1:h-2);
tline = fgets(fid); 
while ~strcmp(tline(1:2),'**'),
 if strfind(tline,'YOUNG')                   
  materials.young=...                        
       sscanf(tline(7:length(tline)),'%f');  % reads young modulus
 elseif strfind(tline,'POISSON')
  materials.poisson=...                      
       sscanf(tline(9:length(tline)),'%f');  % reads poisson coefficient
 elseif strfind(tline,'SIGMA0')
  materials.sigma0=...
       sscanf(tline(8:length(tline)),'%f');  % reads yield limit
 elseif strfind(tline,'HARDENING')
  materials.H=...
      sscanf(tline(11:length(tline)),'%f');  % reads linear hardening parameter
 elseif strfind(tline,'RHO')
  materials.rho=...
       sscanf(tline(5:length(tline)),'%f');  % reads density
 end 
 tline = fgets(fid); 
end
E=materials.young;
nu=materials.poisson;
switch analysis.type
case {'PLANESTRAIN','PLAST','DYNAMIC'}
 materials.A=...                             % defines matrix of elastic constants
      E/((1+nu)*(1-2*nu))*[1-nu,nu,0;
                           nu,1-nu,0;
                           0,0,(1-2*nu)/2];  
case {'PLANESTRESS'}
 materials.A=E/(1-nu^2)*[1,nu,0;
                         nu,1,0;
                         0,0,(1-nu)/2];   
case {'AXI'}

 disp('---')
 disp('Define A matrix for axisymmetric problems in read_input.m line 265')
 disp('---')

otherwise
 disp('Unknown material type')
end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads initial conditions 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function analysis=input_initial(fid,tline,analysis)

tline = fgets(fid); 
while ~strcmp(tline(1:2),'**'),
 if strfind(tline,'VELOCITY')
   h=strfind(tline,'VAL=');                  % reads value of initial velocity
   val=sscanf(tline(h+4:length(tline)),'%f');
   tline=tline(1:h-2);
   h=strfind(tline,'DIR=');                  % reads direction of initial velocity
   dir=sscanf(tline(h+4:length(tline)),'%d');
   tline=tline(1:h-2);
   analysis.invel=zeros(2,1);
   analysis.invel(dir)=val;
 end
 tline = fgets(fid); 
end 

     
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% reads mesh file from GMSH output 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [analysis,elements,corrispnod,coor]=input_mesh(pname,mname)

fmid=fopen([pname mname],'r'); 
tline = fgets(fmid); 
tline = fgets(fmid); 
analysis.NN=sscanf(tline,'%d');
temp=zeros(analysis.NN,1); 
coor=zeros(analysis.NN,2);                   % matrix of cooordinates
for i=1:analysis.NN,
 tline = fgets(fmid); 
 h=sscanf(tline,'%d %f %f %f')';
 temp(i)=h(1);
 coor(i,:)=h(2:3);
end
maxnum=max(temp);
corrispnod=zeros(1,maxnum);                  % correspondence between GMSH ...
ic=find(temp);                               % node numbering and numbering employed by ...
corrispnod(temp)=ic;                         % Matlab codes

tline = fgets(fmid); 
tline = fgets(fmid); 
tline = fgets(fmid); 
analysis.NE=sscanf(tline,'%d');              % gets number of elements ...
elements=repmat(struct...                    % creates a vector of structures ... 
         ('nodes',[],'elset',0,'mat',0),...  % each structure represents one element and ...
         1,analysis.NE);                     % contains the connectivity (nodes), ...
for i=1:analysis.NE,                         % the elset to which the element belongs, ...
 tline = fgets(fmid);                        % and the material (mat)
 h=sscanf(tline,'%d')';
 elements(i).elset=h(3);
 elements(i).nodes=corrispnod(h(6:length(h)));
end
switch h(2)                                  % all the elements in the mesh have same type
case{2}                                      % T3 elements
		analysis.Etag=char('3');                    
		analysis.ne=3;                             % number of nodes in one surface element
  analysis.ns=2;                             % number of nodes in one line element
		analysis.ng=1;                             % number of gauss points
case{9}
		analysis.Etag=char('6');                   % T6 elements 
		analysis.ne=6;                             % number of nodes in one surface element
  analysis.ns=3;                             % number of nodes in one line element
		analysis.ng=3;                             % number of gauss points
end

fclose(fmid);


