
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Sets limits for color plots 
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function [maxsp,minsp,dsp]=limits(infopost,maxre,minre)

if infopost.auto==1,                         % "auto" mode active
 maxsp=maxre; 
 minsp=minre;
else                                         % "manual" mode active
 maxsp=infopost.val(2);                       
 minsp=infopost.val(1);
end
maxsp=max(maxsp,minsp+1.d-4);
dsp=max(maxre-minre,1.d-4);
dsp=1.d-4;
caxis([minsp,maxsp])                         % sets automatic color scaling
set(findobj(gcf,'Tag','RangeValue'),...      % updates text in Post Window
       'String',mat2str([minsp maxsp],5));
