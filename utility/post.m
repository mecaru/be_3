
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% creates postprocessing images in graphical window
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function post(infopost)
infopost
global displ analysis coor connec stress p

xmin=min(coor);
xmax=max(coor);
dx=xmax-xmin;

axis equal;
if infopost.edge==1,  
 line='-';  
else, 
 line='none'; 
end
colormap(jet(256));
cmap=colormap;

switch infopost.ptype,
    
case ' mesh-undef',
    % visualization of the undeformed mesh
 setaxis(xmin,xmax,dx)
 axis off;
 patch('faces',connec(:,1:3),...             % plots undeformed mesh
       'vertices',coor,...
       'FaceColor',[.85 .85 .85],...
       'EdgeColor',[.5 .5 .5])

case ' mesh-def',                            % visualization of the deformed mesh
 mdisp=max(max(abs(displ)));                 % gets max magnitude of displacement compon. 
 coorf=coor+displ/mdisp;                     % computes final coordinates with scaled displ.
 xminf=min(coorf); xmaxf=max(coorf);         % finds max and min final coordinates ...
 dxf=xmaxf-xminf;                            % and difference
 setaxis(xminf,xmaxf,dxf)  
 axis off;
 patch('faces',connec(:,1:3),...             % plots deformed mesh
       'vertices',coorf,...
       'FaceColor',[.85 .85 .85],...
       'EdgeColor',[.5 .5 .5]);

case ' mesh-both'                            % visualization of both meshes
 mdisp=max(max(abs(displ)));                 % (see comments above)
 coorf=coor+displ/mdisp;
 xminf=min([coorf;xmin]); 
 xmaxf=max([coorf;xmax]); 
 dxf=xmaxf-xminf;
 setaxis(xminf,xmaxf,dxf)
 axis off;
 patch('faces',connec(:,1:3),...
       'vertices',coor,...
       'FaceColor',[.85 .85 .85],...
       'LineStyle','none');
 patch('faces',connec(:,1:3),...
       'vertices',coorf,...
       'FaceColor',[.9 .9 .9],...
       'EdgeColor',[.5 .5 .5]);

case {' u1',' u2'}                           % displacement plot
 switch infopost.ptype,                      % selects displacement component 
  case ' u1', idir=1;
  case ' u2', idir=2;
 end     
 setaxis(xmin,xmax,dx)
 axis off;
 maxre=max(displ(:,idir));                   % find max and min values
 minre=min(displ(:,idir));
 [maxsp,minsp,dsp]=...
            limits(infopost,maxre,minre);
 patch('faces',connec(:,1:3),...             % plots colour map
       'vertices',coor,...
       'FaceVertexCData',displ(:,idir),...
       'FaceColor','interp',...
       'LineStyle',line);
 myleg(cmap,minsp,maxsp,minre,maxre,...      % adds legend
       infopost.auto);
       
case {' s11',' s22',' s12'}                  % stress plot
 switch infopost.ptype,
  case ' s11', idir=1;
  case ' s22', idir=2;
  case ' s12', idir=3;
 end     
 setaxis(xmin,xmax,dx)
 axis off;
 maxre=max(stress(:,idir));                  % finds max and min values 
 minre=min(stress(:,idir));
 [maxsp,minsp,dsp]=...
        limits(infopost,maxre,minre);
 patch('faces',connec(:,1:3),...             % plots colour map
       'vertices',coor,...
       'FaceVertexCData',stress(:,idir),...
       'FaceColor','interp',...
       'LineStyle',line);
 myleg(cmap,minsp,maxsp,minre,maxre,...      % adds legend
       infopost.auto);
 
case ' p'                                    % plot of equivalent plastic strain
        
 setaxis(xmin,xmax,dx)
 axis off;
 maxre=max(p);                               % finds max and min values
 minre=min(p);
 [maxsp,minsp,dsp]=...
       limits(infopost,maxre,minre);
 patch('faces',connec(:,1:3),...             % plots colour map
       'vertices',coor,...
       'FaceVertexCData',p,...
       'FaceColor','interp', ...
       'LineStyle',line);
 myleg(cmap,minsp,maxsp,minre,maxre,...      % adds legend
       infopost.auto); 

case ' ld-history',                          % buckling history for large displ ...
                                             % analyses
 fid_disp=fopen('temp/displ.dat','r'); 
 num_step=fread(fid_disp,1,'integer*4');
 setaxis(xmin,xmax,dx)
 axis off;
 for jstep=1:num_step                        % for each load step reads displacements
  displ(:,1)=fread(fid_disp,...
                   analysis.NN,'float32');
  displ(:,2)=fread(fid_disp,...
                   analysis.NN,'float32');
  coorf=coor+displ;
  cla
  patch('faces',connec,...                   % plots undeformed mesh ...
        'vertices',coor,...
        'FaceColor',[.85 .85 .85],...
        'LineStyle','none');
  patch('faces',connec,...                   % and deformed mesh
        'vertices',coorf,...
        'FaceColor',[.9 .9 .9],...
        'EdgeColor',[.5 .5 .5]);
  pause(.2)  
 end
 fclose(fid_disp);

end


%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% function setaxis
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function setaxis(vecmin,vecmax,vecdelta)


center=1/2*(vecmin+vecmax);

dmax=1.1*max(vecdelta)/2;
axis([center(1)-dmax center(1)+dmax ...
      center(2)-dmax center(2)+dmax]);

