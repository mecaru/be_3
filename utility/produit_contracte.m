%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
% Calcul du produit contract� entre trois matrices
%!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
function prod=produit_contracte(a,b,c)
% Les matrices a et b sont de tailles 2X2, la matrice c de taille 3X3
vec_a = [a(1,1) a(2,2) a(1,2)+a(2,1)];
vec_c = [c(1,1) ; c(2,2) ; c(1,2)+c(2,1)];
prod = vec_a*b*vec_c;
end

